// --- Directions
// Check to see if two provided strings are anagrams of eachother.
// One string is an anagram of another if it uses the same characters
// in the same quantity. Only consider characters, not spaces
// or punctuation.  Consider capital letters to be the same as lower case
// --- Examples
//   anagrams('rail safety', 'fairy tales') --> True
//   anagrams('RAIL! SAFETY!', 'fairy tales') --> True
//   anagrams('Hi there', 'Bye there') --> False

function anagrams(stringA, stringB) {
    stringA = cleanString(stringA);
    stringB = cleanString(stringB);
    if (stringA === stringB) {
        return true;
    } else {
        return false;
    }
}

function cleanString(str) {
    str = str.split('');
    let newString = [];
    let alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    for (let index=0; index<str.length; index++) {
        if (alphabet.indexOf(str[index]) >= 0) {
            newString.push(str[index]);
        }
    }
    newString = newString.sort().join('');
    return newString;
}

module.exports = anagrams;
