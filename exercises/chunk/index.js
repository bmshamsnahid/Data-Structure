// --- Directions
// Given an array and chunk size, divide the array into many subarrays
// where each subarray is of length size
// --- Examples
// chunk([1, 2, 3, 4], 2) --> [[ 1, 2], [3, 4]]
// chunk([1, 2, 3, 4, 5], 2) --> [[ 1, 2], [3, 4], [5]]
// chunk([1, 2, 3, 4, 5, 6, 7, 8], 3) --> [[ 1, 2, 3], [4, 5, 6], [7, 8]]
// chunk([1, 2, 3, 4, 5], 4) --> [[ 1, 2, 3, 4], [5]]
// chunk([1, 2, 3, 4, 5], 10) --> [[ 1, 2, 3, 4, 5]]

function chunk(array, size) {
    let finalArray = [],
        chunkArray = [],
        chunkCount = 0;
    for (let index=0; index<array.length; index++) {
        if (chunkCount == size) {
            finalArray.push(chunkArray);
            chunkArray = [];
            chunkCount = 0;
        }
        chunkCount ++;
        chunkArray.push(array[index]);
    }
    if (chunkArray.length > 0) {
        finalArray.push(chunkArray);
    }
    return finalArray;
}

module.exports = chunk;
