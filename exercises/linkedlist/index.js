// --- Directions
// Implement classes Node and Linked Lists
// See 'directions' document

class Node {
    constructor (data, next = null) {
        this.data = data;
        this.next = next;
    }
}

class LinkedList {
    constructor () {
        this.head = null;
    }

    insertFirst (data) {
        let node = new Node(data);
        if (this.head != null) {
            node.next = this.head;
        }
        this.head = node;
    }

    size () {
        let count = 0;
        let head = this.head;
        while (head != null) {
            head = head.next;
            count++;
        }
        return count;
    }

    getFirst () {
        return this.head;
    }

    getLast () {
        let head = this.head;
        let previousNode;
        while (head != null) {
            previousNode = head;
            head = head.next;
        }
        return previousNode;
    }

    clear () {
        this.head = null;
    }

    removeFirst () {
        this.head = this.head.next;
    }

    removeLast () {
        if (this.head == null) {
            return;
        }
        if (this.head.next == null) {
            this.head = null;
            return;
        }
        let currentNode = this.head;
        let previousNode;
        while (currentNode.next) {
            previousNode = currentNode;
            currentNode = currentNode.next;
        }
        previousNode.next = null;
    }

    insertLast (data) {
        let newNode = new Node (data, null);
        if (!this.head) {
            this.head = newNode;
        }
    }
}

module.exports = { Node, LinkedList };
