// --- Directions
// Given a string, return the character that is most
// commonly used in the string.
// --- Examples
// maxChar("abcccccccd") === "c"
// maxChar("apple 1231111") === "1"

function maxChar(str) {
    let myMap = {};
    str = str.split('');

    let myCharacter = str[0];
    let characterNumber = 0;

    for (let index=0; index<str.length; index++) {
        if (typeof myMap[str[index]] != 'undefined') {
            myMap[str[index]] ++;
        } else {
            myMap[str[index]] = 0;
        }
        if (myMap[str[index]] > characterNumber) {
            characterNumber = myMap[str[index]];
            myCharacter = str[index];
        }
    }

    return myCharacter;
}

module.exports = maxChar;
