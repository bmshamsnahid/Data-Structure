// --- Directions
// Given a string, return true if the string is a palindrome
// or false if it is not.  Palindromes are strings that
// form the same word if it is reversed. *Do* include spaces
// and punctuation in determining if the string is a palindrome.
// --- Examples:
//   palindrome("abba") === true
//   palindrome("abcdefg") === false

function palindrome(str) {
    str = str.split('');
    let size = str.length;
    let iteration = size / 2;
    for (let index=0; index<=iteration; index++) {
        if (str[index] != str[size-index-1]) {
            return false;
        }
    }
    return true;
}

module.exports = palindrome;
