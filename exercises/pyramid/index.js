// --- Directions
// Write a function that accepts a positive number N.
// The function should console log a pyramid shape
// with N levels using the # character.  Make sure the
// pyramid has spaces on both the left *and* right hand sides
// --- Examples
//   pyramid(1)
//       '#'
//   pyramid(2)
//       ' # '
//       '###'
//   pyramid(3)
//       '  #  '
//       ' ### '
//       '#####'
//   pyramid(4)
//       '   #   '
//       '  ###  '
//       ' ##### '
//       '#######'

function pyramid(n) {
    let noOfHash = 1;
    let str = '';
    for (let index=1; index<=n; index++) {
        str = generateHash(noOfHash);
        noOfHash += 2;
        str = printSpaceBefore(n - index, str);
        str = printSpaceAfter(n - index, str);
        console.log(str);
    }
}

function generateHash(n) {
    let str = "";
    for (let index=0; index<n; index++) {
        str += '#';
    }
    return str;
}

function printSpaceBefore(n, str) {
    for (let index=0; index<n; index++) {
        str = ' ' + str;
    }
    return str;
}

function printSpaceAfter(n, str) {
    for (let index=0; index<n; index++) {
        str = str + ' ';
    }
    return str;
}

module.exports = pyramid;
