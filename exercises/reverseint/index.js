// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9

function reverseInt(n) {
    let isNegative = false;
    let newNumber = 0;

    if (n < 0) {
        isNegative = true;
        n = n * -1;
    }

    while (n > 10) {
        newNumber = newNumber * 10;
        newNumber += (n % 10);
        n = parseInt(n / 10);
    }

    newNumber = newNumber * 10;
    newNumber += n;

    if (isNegative) {
        newNumber = newNumber * -1;
    }

    return newNumber;
}

module.exports = reverseInt;
